import axios from 'axios'

export default function install ({ app, router, store, Vue }) {
  Vue.prototype.$apiGet = async (urlPath, authRequired = true) => {
    if (authRequired && !store.getters['sphere/isLoggedIn']) {
      store.dispatch('sphere/logout')
      return null
    }

    const url = `${Vue.prototype.$host}/${urlPath}`
    let data
    try {
      const requestConfig = authRequired ? { headers: { Authorization: `${store.state.sphere.jwtToken}` } } : {}
      data = (await axios.get(url, requestConfig)).data
    } catch (err) {
      if (err.response.status === 401 || err.response.statusText === 'Unauthorized') {
        store.dispatch('sphere/logout')
      }
      return null
    }
    return data
  }

  Vue.prototype.$apiPost = async (urlPath, params, authRequired = true) => {
    if (authRequired && !store.getters['sphere/isLoggedIn']) {
      store.dispatch('sphere/logout')
      return null
    }

    const url = `${Vue.prototype.$host}/${urlPath}`
    let data
    try {
      const requestConfig = authRequired ? { headers: { Authorization: `${store.state.sphere.jwtToken}` } } : {}
      data = (await axios.post(url, params, requestConfig)).data
    } catch (err) {
      if (err.response.status === 401 || err.response.statusText === 'Unauthorized') {
        store.dispatch('sphere/logout')
      }
      return null
    }
    return data
  }
}
