export default ({ Vue }) => {
  Vue.prototype.$host = process.env.DEV ? '/api' : '/api'

  Vue.prototype.$goalsRus = {
    hireWorker: 'НАНЯТЬ ЛЮДЕЙ',
    findJob: 'НАЙТИ РАБОТУ',
    findFriends: 'НАЙТИ ДРУЗЕЙ',
    findInvestors: 'НАЙТИ ИНВЕСТОРОВ'
  }

  Vue.prototype.$meetingWaysRus = {
    coffee: 'КОФЕ',
    lunch: 'ОБЕД',
    web: 'WEB',
    phone: 'ТЕЛЕФОН'
  }
}
