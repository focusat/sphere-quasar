var firebase = require('firebase')

var config = {
  apiKey: 'AIzaSyDCAh5zZYd1DyKzPRVCMxbV5xaBYhCgxPQ',
  authDomain: 'sphere-85aff.firebaseapp.com',
  databaseURL: 'https://sphere-85aff.firebaseio.com',
  projectId: 'sphere-85aff',
  storageBucket: 'sphere-85aff.appspot.com',
  messagingSenderId: '1075936290261'
}
firebase.initializeApp(config)
firebase.auth().useDeviceLanguage()

export default ({ Vue }) => {
  Vue.prototype.$firebase = firebase
}
