
const routes = [
  {
    path: '/bookmarks',
    component: () => import('layouts/tabs.vue'),
    children: [
      {
        path: '',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'Мои закладки',
          hash: '/bookmarks'
        },
        component: () => import('pages/Bookmarks.vue')
      }
    ]
  },
  {
    path: '/settings',
    component: () => import('layouts/tabs.vue'),
    children: [
      { path: '', component: () => import('pages/UserCard.vue'), meta: { requiresLogin: true } }
    ]
  },
  {
    path: '/surf',
    component: () => import('layouts/tabs.vue'),
    children: [
      { path: '', component: () => import('pages/UserPage.vue'), meta: { requiresLogin: true } }
    ]
  },
  {
    path: '/chats',
    component: () => import('layouts/tabs.vue'),
    children: [
      {
        path: '',
        meta: {
          requiresLogin: true // ,
          // header: true,
          // title: 'Чаты',
          // hash: '/chats'
        },
        component: () => import('pages/Chats.vue')
      }
    ]
  },
  {
    path: '/chat',
    component: () => import('layouts/tabs.vue'),
    children: [
      {
        path: '',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'alex.rogers',
          hash: '/chat',
          showRoute: true,
          backRoute: '/chats'
        },
        component: () => import('pages/Chat.vue')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/tabs.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/tabs.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/tabs.vue'),
    children: [
      {
        path: '/',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'Профиль',
          hash: '/profile'
        },
        component: () => import('pages/Profile.vue')
      },
      {
        path: '/mettingSettings',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'Варианты встречи',
          hash: '/mettingSettings',
          showRoute: true,
          backRoute: '/profile',
          showRightButton: true,
          rightButtonLabel: 'Готово'
        },
        component: () => import('pages/MeetingSettings.vue')
      },
      {
        path: '/editInterests',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'Варианты встречи',
          hash: '/editInterests',
          showRoute: true,
          backRoute: '/profile',
          showRightButton: true,
          rightButtonLabel: 'Готово'
        },
        component: () => import('pages/Interests.vue')
      },
      {
        path: '/editPortfolio',
        meta: {
          requiresLogin: true,
          header: true,
          title: 'Обо мне',
          hash: '/editPortfolio',
          showRoute: true,
          backRoute: '/profile',
          showRightButton: true,
          rightButtonLabel: 'Готово'
        },
        component: () => import('pages/EditPortfolio.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
