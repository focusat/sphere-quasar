import Vue from 'vue'
import Vuex from 'vuex'

import sphere from './sphere'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      sphere
    }
  })

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./sphere'], () => {
      const newSphere = require('./sphere').default
      Store.hotUpdate({ modules: { showcase: newSphere } })
    })
  }

  return Store
}
