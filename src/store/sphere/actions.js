// import Vue from 'vue'
import Router from 'vue-router'

const ID_TOKEN_KEY = 'id_token'
const router = new Router()

export function loadJwtToken ({ commit }) {
  // if (this.state.jwtToken === null) {
  const storedJwtToken = localStorage.getItem(ID_TOKEN_KEY)
  if (storedJwtToken !== null) {
    commit('setJWT', storedJwtToken)
  } else {
    commit('setJWT', '')
  }
  // }
}

export async function login ({ commit, state, getters }, /* { username, password } */{ jwtToken }) {
  // // Perform the HTTP request.
  // const res = await Vue.prototype.$apiPost(`login`, { username, password }, false)
  // // Calls the mutation defined to update the state's JWT.
  // if (res.success === 'error') {
  //   return
  // }

  await commit('setJWT', jwtToken/* await res.token */)
  if (getters['sphere/isLoggedIn']) {
    // tslint:disable-next-line:
    router.push({path: router.currentRoute.query.next})
  }
}

export async function logout ({ commit }) {
  commit('setJWT', '')
  localStorage.removeItem(ID_TOKEN_KEY)
  router.push({path: `login`})
}
