import decode from 'jwt-decode'

export function isLoggedIn (state) {
  return state.jwtToken !== null && !isTokenExpired(state.jwtToken)
}

function isTokenExpired (encodedToken) {
  if (encodedToken === '' || encodedToken === null) {
    return true
  }

  let token
  try {
    token = decode(encodedToken)
    if (!token.exp) { return true }
  } catch (err) {
    return true
  }

  const expirationDate = new Date(0)
  expirationDate.setUTCSeconds(token.exp)

  return expirationDate < new Date()
}
