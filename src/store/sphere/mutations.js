export const updatePageMeta = (state, meta) => {
  state.pageMeta = {
    header: false,
    title: '',
    hash: '',
    icon: '',
    iframeTabs: false,
    showRoute: false,
    backRoute: '/',
    showRightButton: false,
    rightButtonLabel: '',
    tabs: [],
    ...meta
  }
}

const ID_TOKEN_KEY = 'id_token'
export function setJWT (state, jwt) {
  // When this updates, the getters and anything bound to them updates as well.
  state.jwtToken = jwt
  localStorage.setItem(ID_TOKEN_KEY, state.jwtToken)
}

export function setJwtByStorageIfNotLoggedIn (state) {
  if (state.jwtToken === null) {
    const storedJwtToken = localStorage.getItem(ID_TOKEN_KEY)
    state.jwtToken = storedJwtToken
  }
}

export function setProfileData (state, profile) {
  state.profile = profile
}
