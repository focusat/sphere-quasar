export default {
  jwtToken: null,
  pageMeta: {},
  profile: {
    portfolio: '',
    vk: '',
    twitter: '',
    fb: '',
    instagram: ''
  }
}
